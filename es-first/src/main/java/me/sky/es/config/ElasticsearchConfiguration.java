package me.sky.es.config;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by sky
 */
@Configuration
public class ElasticsearchConfiguration {

    @Value("${elasticsearch.host}")
    private String eshost;

    @Value("${elasticsearch.port}")
    private String esport;

    @Value("${elasticsearch.clustername}")
    private String clustername;

    @Bean
    public TransportClient client() {
        TransportClient client =null;
        try {
            Settings settings = Settings.builder().put("cluster.name", clustername).put("client.transport.sniff", true)
                    .build();

            client = new PreBuiltTransportClient(settings);
            client.addTransportAddress(
                    new InetSocketTransportAddress(InetAddress.getByName(eshost),
                            Integer.parseInt(esport)));

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return client;
    }

}
