package me.sky.es.controller;


import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.transport.TransportClient;

import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


/**
 * Created by sky
 */
@RestController
@RequestMapping("/es")
public class Escontroller {

    @Autowired
    private TransportClient client;

    /**
     * 新增员工
     *
     * @throws IOException
     */
    @PostMapping("/add")
    public ResponseEntity add(@RequestBody Employee employee) throws IOException {

        IndexRequestBuilder indexRequestBuilder = client.prepareIndex("company1", "employee1", employee.getId());
        XContentBuilder xContentBuilder = XContentFactory.jsonBuilder()
                .startObject()
                .field("name1", employee.getName())
                .field("age1", employee.getAge())
                .field("position1", employee.getPosition())
                .field("salary1", employee.getSalary())
                .endObject();
        IndexResponse indexResponse = indexRequestBuilder.setSource(xContentBuilder).get();
        return ResponseEntity.ok(indexResponse.getResult());

    }

    /**
     * 查询员工
     */
    @PostMapping("/query")
    public ResponseEntity query(@RequestBody Employee employee) {
        GetResponse response = client.prepareGet("company", "employee", employee.getId()).get();
        return ResponseEntity.ok(response.getSourceAsString());
    }

    /**
     * 删除
     */
    @PostMapping("delete")
    public ResponseEntity delete(@RequestBody Employee employee) {
        DeleteResponse response = client.prepareDelete("company", "employee", employee.getId()).get();
        return ResponseEntity.ok(response.getResult());
    }

    /**
     * 修改
     */
    @PostMapping("update")
    public ResponseEntity update(@RequestBody Employee employee) throws IOException {
        UpdateResponse response = client.prepareUpdate("company", "employee", employee.getId())
                .setDoc(XContentFactory.jsonBuilder()
                        .startObject()
                        .field("position", employee.getPosition())
                        .field("salary",employee.getSalary())
                        .endObject())
                .get();
        return ResponseEntity.ok(response.getResult());
    }


    /**
     * 条件查询
     */
    @PostMapping("/conditionQuery")
    public void conditionQuery() {
        QueryBuilder queryBuilder = QueryBuilders.queryStringQuery("ipad");
        SearchResponse searchResponse = client.prepareSearch("esindex")
                .setQuery(queryBuilder)//设置查询条件
                .setFrom(0)
                .setSize(60)
                .execute()
                .actionGet();
        //SearchHits表示这是一个列表
        SearchHits hits = searchResponse.getHits();
        System.out.println("总共有：" + hits.getHits().length);
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
        client.close();

    }

}




    



