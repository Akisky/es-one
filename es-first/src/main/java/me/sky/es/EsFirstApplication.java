package me.sky.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 程序启动的入口
 */
@SpringBootApplication
@EnableAutoConfiguration
public class EsFirstApplication {

	public static void main(String[] args) {
		SpringApplication.run(EsFirstApplication.class, args);
	}
}
