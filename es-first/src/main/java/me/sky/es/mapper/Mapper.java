package me.sky.es.mapper;

import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.IOException;

/**
 * Created by sky
 */
public class Mapper {

    public static XContentBuilder getMapping() throws IOException {
        XContentBuilder mapping = XContentFactory.jsonBuilder().startObject()
                //.startObject("test")
                .startObject("properties")
                .startObject("id").field("type", "long").field("store", "yes").endObject()

                .startObject("type").field("type", "string").field("index", "not_analyzed")
                .endObject()
                .startObject("catIds")
                .field("type", "integer")
                .endObject()
                .endObject()
                .endObject();
        return mapping;


    }

}
